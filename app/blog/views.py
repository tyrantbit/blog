from django.views.generic import TemplateView, DetailView, ListView
from .models import Post

# Create your views here.
class PostList(ListView):
    queryset = Post.objects.filter(status=True).order_by('-created_on')
    template_name = 'post_list.html'


class PostDetail(DetailView):
    model = Post
    template_name = 'post_detail.html'